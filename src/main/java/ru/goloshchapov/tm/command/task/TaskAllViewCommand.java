package ru.goloshchapov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public final class TaskAllViewCommand extends AbstractCommand {

    @NotNull public static final String NAME = "task-view-all";

    @NotNull public static final String DESCRIPTION = "Show all tasks";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ALL TASKS]");
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        int index = 1;
        for (@NotNull final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
