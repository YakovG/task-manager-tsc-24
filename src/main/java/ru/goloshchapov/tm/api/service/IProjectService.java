package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project add(String userId, String name, String description);

}
