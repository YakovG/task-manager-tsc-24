package ru.goloshchapov.tm.exception.auth;

import ru.goloshchapov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access is denied...");
    }

}
