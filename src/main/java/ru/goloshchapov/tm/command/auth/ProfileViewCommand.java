package ru.goloshchapov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.model.User;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class ProfileViewCommand extends AbstractCommand {

    @NotNull public static final String NAME = "profile-view";

    @NotNull public static final String DESCRIPTION = "View user profile";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        @Nullable String name = user.getFirstname();
        if (isEmpty(name)) name = "Undefined";
        System.out.println("FIRST NAME: " + name);
        name = user.getLastname();
        if (isEmpty(name)) name = "Undefined";
        System.out.println("LAST NAME: " + name);
        name = user.getMiddlename();
        if (isEmpty(name)) name = "Undefined";
        System.out.println("MIDDLE NAME: " + name);
    }
}
