package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getCommandList();

    Collection<String> getListCommandNames();

    Collection<String> getListCommandArgs();
}
