package ru.goloshchapov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

public interface ICommandRepository {


    @NotNull List<AbstractCommand> getCommandList();

    Collection<String> getCommandNames();

    Collection<String> getArgumentNames();
}
