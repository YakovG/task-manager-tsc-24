package ru.goloshchapov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ICommandRepository;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.api.service.*;
import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.exception.system.UnknownArgumentException;
import ru.goloshchapov.tm.exception.system.UnknownCommandException;
import ru.goloshchapov.tm.repository.CommandRepository;
import ru.goloshchapov.tm.repository.ProjectRepository;
import ru.goloshchapov.tm.repository.TaskRepository;
import ru.goloshchapov.tm.repository.UserRepository;
import ru.goloshchapov.tm.service.*;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class Bootstrap implements ServiceLocator {

    @NotNull private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        declareCommands(commandService.getCommandList());
    }

    private void declareCommands (@NotNull List<AbstractCommand> commands) {
        for (@NotNull AbstractCommand command : commands) {
            registry(command);
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable final AbstractCommand command = arguments.get(arg);
        if (command == null) throw new UnknownArgumentException(arg);
        command.execute();
    }

    private void parseCommand(@Nullable final String cmd) {
        if (isEmpty(cmd)) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    private boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        try {
            parseArg(arg);
        } catch (final Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

    public void run(String... args) {
        final Runnable messageFail = () -> System.err.println("[FAIL]");
        final Runnable messageOk = () -> System.out.println("[OK]");
        final Runnable messageCommand = () -> System.out.println("[ENTER COMMAND]");
        final Predicate<String> checkDefault = "y"::equals;
        loggerService.debug("DEBUG INFO!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        System.out.println("CREATE DEFAULT USERS? (y)");
        if (checkDefault.test(TerminalUtil.nextLine())) {
            parseCommand("user-create-by-default");
            loggerService.info("DEFAULT USERS CREATED");
        }
        System.out.println("USE TEST DATASET? (y)");
        if (checkDefault.test(TerminalUtil.nextLine())) {
            parseCommand("create-test-data");
            loggerService.info("TEST DATASET CREATED");
        }
        messageCommand.run();
        while (true) {
            @Nullable final String cmd = TerminalUtil.nextLine();
            loggerService.command(cmd);
            try {
                parseCommand(cmd);
                messageOk.run();
            } catch (final Exception e) {
                loggerService.error(e);
                messageFail.run();
                messageCommand.run();
            }
        }
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() { return projectTaskService; }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }
}
