package ru.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<M extends AbstractBusinessEntity>
        extends AbstractRepository<M> implements IBusinessRepository<M> {

    public Predicate<M> predicateByUserId(@NotNull final String userId) {
        return m -> m.checkUserAccess(userId);
    }

    public Predicate<M> predicateById(@NotNull final String userId, @NotNull final String modelId) {
        return m -> m.checkUserAccess(userId) && modelId.equals(m.getId());
    }

    public Predicate<M> predicateByName(@NotNull final String userId, @NotNull final String name) {
        return m -> m.checkUserAccess(userId) && name.equals(m.getName());
    }

    public Predicate<M> predicateByStarted(@NotNull final String userId) {
        return m -> m.checkUserAccess(userId) && (m.getStatus() != Status.NOT_STARTED);
    }

    public Predicate<M> predicateByCompleted(@NotNull final String userId) {
        return m -> m.checkUserAccess(userId) && (m.getStatus() == Status.COMPLETE);
    }

    @NotNull
    private M start (@NotNull final M m) {
        m.setStatus(Status.IN_PROGRESS);
        m.setDateStart(new Date());
        return m;
    }

    @NotNull
    private M finish (@NotNull final M m) {
        m.setStatus(Status.COMPLETE);
        m.setDateFinish(new Date());
        return m;
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        list.add(model);
        return model;
    }

    @Override
    public void addAll(@NotNull final String userId, @Nullable final Collection<M> collection) {
        if (collection == null) return;
        collection.forEach(c->c.setUserId(userId));
        list.addAll(collection);
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAllByUserId(@NotNull final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAllStarted(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        return list.stream()
                .filter(predicateByStarted(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAllCompleted(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        return list.stream()
                .filter(predicateByCompleted(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String modelId) {
        return list.stream()
                .filter(predicateById(userId, modelId))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final M model = list.get(index);
        if (model.checkUserAccess(userId)) return model;
        return null;
    }

    @Nullable
    @Override
    public M findOneByName(@NotNull final String name) {
        return list.stream()
                .filter(m -> name.equals(m.getName()))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByName(@NotNull final String userId, @NotNull final String name) {
        return list.stream()
                .filter(predicateByName(userId, name))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Override
    public boolean isAbsentByName(@NotNull final String name) {
        return findOneByName(name) == null;
    }

    @Nullable
    @Override
    public String getIdByName(final String name) {
        @Nullable final M model = findOneByName(name);
        if (model == null) return null;
        return model.getId();
    }

    @Override
    public int size(@NotNull final String userId) {
        @Nullable final List<M> models = findAll(userId);
        if (models == null) return 0;
        return models.size();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (model.checkUserAccess(userId)) list.remove(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList())
                .forEach(this::remove);
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String userId, @NotNull final String modelId) {
        final Optional<M> model = Optional.ofNullable(findOneById(userId,modelId));
        model.ifPresent(list::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(list::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeOneByName(@NotNull final String userId, @NotNull final String name) {
        final Optional<M> model = Optional.ofNullable(findOneByName(userId, name));
        model.ifPresent(list::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M startOneById(@NotNull final String userId, @NotNull final String modelId) {
        final Optional<M> model = Optional.ofNullable(findOneById(userId, modelId));
        return model.map(this::start).orElse(null);
    }

    @Nullable
    @Override
    public M startOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        return model.map(this::start).orElse(null);
    }

    @Nullable
    @Override
    public M startOneByName(@NotNull final String userId, @NotNull final String name) {
        final Optional<M> model = Optional.ofNullable(findOneByName(userId, name));
        return model.map(this::start).orElse(null);
    }

    @Nullable
    @Override
    public M finishOneById(@NotNull final String userId, @NotNull final String modelId) {
        final Optional<M> model = Optional.ofNullable(findOneById(userId, modelId));
        return model.map(this::finish).orElse(null);

    }

    @Nullable
    @Override
    public M finishOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        return model.map(this::finish).orElse(null);
    }

    @Nullable
    @Override
    public M finishOneByName(@NotNull final String userId, @NotNull final String name) {
        final Optional<M> model = Optional.ofNullable(findOneByName(userId, name));
        return model.map(this::finish).orElse(null);
    }

}
